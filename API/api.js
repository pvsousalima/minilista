import {
    Alert,
    RefreshControl,
    Platform,
    AsyncStorage
} from 'react-native'

import Contacts from 'react-native-contacts'

// Saves to AsyncStorage
export const cacheToAsyncStorage = (key, value) => {
    return AsyncStorage.setItem('@MySuperStore:'+key, JSON.stringify(value))
}

// Reads from AsyncStorage
export const readFromAsyncStorage = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('@MySuperStore:'+key).then((item) => {
            resolve(JSON.parse(item))
        })
    })
}

// Merge with AsyncStorage
export const mergeWithAsyncStorage = (key, value) => {
    return AsyncStorage.mergeItem('@MySuperStore:'+key, JSON.stringify(value))
}


// A custom toast alert
export const customToastAlert = (title, message) => {
    Alert.alert(
        title,
        message,
        { cancelable: true }
    )
}

// mas for telephones
export const mtel = (v) => {
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

export const addWhatsAppAloBrasil = () => {
    readFromAsyncStorage('contato').then((read) => {
        if(!read){
            cacheToAsyncStorage('contato', true).then(() => {
                const ContatoAloBrasil = {
                    phoneNumbers: [{
                        label: "mobile",
                        number: "(555) 555-5555",
                    }],
                    familyName: 'Brasil Comunicação',
                    givenName: Platform.OS === 'ios' ? 'Alô' : 'Alô Brasil Comunicação',
                }
                Contacts.addContact(ContatoAloBrasil, (err) => {
                    err ? customToastAlert('Erro', 'Não foi possível adicionar o contato WhatsApp do Alô Brasil aos seus contatos! :(') : customToastAlert('Sucesso!', 'Contato WhatsApp do Alô Brasil foi adicionado aos seus contatos!')
                })
            })
        }
    })
}

export const configureOptionsAlerts = () => {
    readFromAsyncStorage('configureOptionsAlert').then((read) => {
        if(!read){
            cacheToAsyncStorage('configureOptionsAlert', true).then(() => {
                !read ? customToastAlert('Aviso', 'Você pode configurar o aplicativo com seus dados no meu ao lado!') : null
                !read ? customToastAlert('Aviso', 'Deslize a lista para baixo para atualizar sua base de dados.') : null
            })
        }
    })
}

// Recupera os contatos do usuario
export const getContacts = () => {
    return new Promise((resolve, reject) => {
        Contacts.getAll((err, contacts) => {
            if (err) {
                reject(err)
            } else {
                resolve(contacts)
            }
        })
    })
}

// getContacts().then((contacts) => {
//     if(contacts.type && contacts.type === 'permissionDenied'){
//         Alert.alert(
//             'Could not access your contacts',
//             'My Alert Msg',
//             { cancelable: true }
//         )
//     } else {
//         AsyncStorage.setItem('@MySuperStore:contacts', JSON.stringify(contacts)).then((saved) => {
//         })
//     }
// })

// default api actions when the app starts
addWhatsAppAloBrasil()
configureOptionsAlerts()
