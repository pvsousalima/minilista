import React, {PureComponent} from 'react'
import {
    AsyncStorage,
    ActivityIndicator,
    Alert,
    Button,
    Dimensions,
    AppRegistry,
    Text,
    View,
    ListView,
    RefreshControl,
    Platform,
    StatusBar,
    TouchableOpacity,
    ScrollView
} from 'react-native'

import {readFromAsyncStorage, cacheToAsyncStorage} from '../API'
import {RecentCallsScreenCard} from './Components'
import Communications from 'react-native-communications';
import Icon from 'react-native-vector-icons/MaterialIcons'
var shortid = require('shortid');

let {width, height} = Dimensions.get('window')

const CustomHeader = ({...props}) => (
    <View style={{backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10}}>
        <View style={{ alignItems:'flex-start', 'flexDirection': 'row', top:2}}>
            <TouchableOpacity style={{marginLeft:15, width:60, height:30, top:5}} onPress={() => {console.log('opress')}}>
                <Text style={{color:'white', fontSize:17}}>Limpar</Text>
            </TouchableOpacity>
        </View>
    </View>
)

class RecentCallsScren extends React.PureComponent {

    static navigationOptions = {
        header: (props) => <CustomHeader {...props}/>,
        title: 'Histórico',
        tabBarIcon: ({ tintColor }) => (
            <Icon name="history" size={28} color={tintColor} />
        ),
    }

    constructor() {
        super()
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = { historyList: [], dataSource: this.ds.cloneWithRows([]) }
    }

    componentWillMount(){
        this.fetchHistory()
    }

    fetchHistory(){
        readFromAsyncStorage('historyList').then((history) => {
            this.setState({dataSource: this.state.dataSource.cloneWithRows(history), historyList: (history)})
        }).catch((err) => {
            this.setState({dataSource: this.state.dataSource.cloneWithRows([]), historyList: []})
        })
    }

    render() {
        return (
            <View style={{flex:1, backgroundColor:'white', flexDirection:'column'}}>
                {this.state.historyList.length > 0 ?
                    (<ListView
                        decelerationRate = 'normal'
                        enableEmptySections
                        showsVerticalScrollIndicator={true}
                        removeClippedSubviews={false}
                        dataSource={this.state.dataSource}
                        renderRow={(data) =>
                            <RecentCallsScreenCard key={shortid.generate()} {...data} onPress={() => {
                                Communications.phonecall(data.numberCalled, false)
                            }}/>
                        }
                     />
                    ) : (<View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'600'}}>Não há chamadas anteriores</Text>
                    </View>)
                }
            </View>
    )
}

}

export default RecentCallsScren
