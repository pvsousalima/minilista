import React, {PureComponent} from 'react'

import {
    AsyncStorage,
    ActivityIndicator,
    Alert,
    Dimensions,
    AppRegistry,
    FlatList,
    Text,
    ListView,
    View,
    RefreshControl,
    Platform,
    StatusBar,
    TouchableOpacity,
    ScrollView
} from 'react-native'

import {ContactsListCard} from './Components'
import {Button, SearchBar} from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Contacts from 'react-native-contacts'
import TableView from 'react-native-tableview'
import shortid from 'shortid'

const {width, height} = Dimensions.get('window')
const Section = TableView.Section;
const Item = TableView.Item;

class AllContactsScreen extends React.PureComponent {

    static navigationOptions = ({ navigation, screenProps }) => ({
        title: 'Contatos',
        header: null,
        tabBarIcon: ({ tintColor }) => (
            <Icon name="contact-phone" size={25} color={tintColor} />
        )
    })

    constructor() {
        super()
        this.state = {
            contactsList: [],
            backupList: [],
            isLoading: true,
            clearIcon: {color: 'transparent'}
        }
    }

    componentWillMount(){
        this.getContacts()
    }

    getContacts(){
        return new Promise((resolve, reject) => {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    reject(err)
                } else {
                    this.setState({contactsList: contacts, backupList:contacts, isLoading:false})
                }
            })
        })
    }

    keyExtractor = (item, index) => item.recordID;

    renderItem({item}) {
        return <ContactsListCard {...item} onPress={() => {
            this.props.navigation.navigate('ProfileScreen', item)
        }}/>
    }

    renderResults(text) {
        if(text.length > 0) {
            let newContacts = []
            this.state.contactsList.map((el) => {
                if(text !== '' && el.givenName.toLowerCase().includes(text.toLowerCase())){
                    newContacts.push(el)
                }
            })
            this.setState({contactsList: newContacts})
        } else {
            this.setState({contactsList: this.state.backupList})
        }
    }

    render() {
        return (
            <View style={{flex:1, backgroundColor:'white'}}>

                <StatusBar
                    backgroundColor="#eda631"
                    barStyle="light-content"
                />

                <View style={{backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10}}>

                    <View style={{ alignItems:'flex-start', 'flexDirection': 'row', top:2}}>
                        <TouchableOpacity style={{marginLeft:15, width:30, height:30, top:3}}>
                            <Icon name="add" size={25} style={{justifyContent:'center'}} color={'white'}/>
                        </TouchableOpacity>

                        <SearchBar
                            lightTheme
                            containerStyle={{borderRadius: 5, height: 30, width:width-70, backgroundColor:'white', backgroundColor:'white', marginRight:10, marginLeft:15}}
                            inputStyle={{backgroundColor: 'white', height: 15}}
                            icon={{color: '#86939e', name: 'search', style:{top:7} }}
                            clearIcon={this.state.clearIcon}
                            placeholder='Pesquisar nos meus contatos'
                            textInputRef='contato'
                            onFocus={() => {
                                this.setState({clearIcon: {color: '#86939e', name: 'clear', style:{top:7}}})
                            }}
                            onBlur={()=> {
                                this.setState({clearIcon: {color: 'transparent', name: 'clear'}})
                            }}
                            onChangeText={(text) => { this.renderResults(text) }}
                        />
                    </View>
                </View>

                { this.state.contactsList.length > 0 ?
                    <TableView style={{flex:1}}
                        tableViewStyle={TableView.Consts.Style.Grouped}
                        tableViewCellStyle={TableView.Consts.CellStyle.Subtitle}
                        reactModuleForCell="TableViewExample"
                        onPress={(item) =>  this.props.navigation.navigate('ProfileScreen', item)}>

                        <Section canEdit={false} arrow={true}>
                            {this.state.contactsList.map((item, index) => {
                                return (
                                    <Item key={"i" + index}
                                        label={`${item.givenName} ${item.familyName}`}
                                        givenName={item.givenName}
                                        familyName={item.familyName}/>
                                )
                            })}
                        </Section>
                    </TableView>
                    :
                    <View style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'#3b5885'}}>
                        <Text style={{ color:'white', fontWeight:'600'}}>
                            Não encontrou nos seus contatos?
                        </Text>
                        <Button
                            textStyle={{color: '#3b5885'}}
                            buttonStyle={{backgroundColor:'white', margin:30, borderRadius:4}}
                            icon={{color: '#3b5885', name: 'search', type: 'MaterialIcons'}}
                            title='Pesquise na minilista!'
                            onPress={() => {
                                this.props.navigation.navigate("Home")
                            }}
                        />
                    </View>
                }
            </View>
        )
    }

}

export default AllContactsScreen
