import React, {PureComponent} from 'react'
import {
    AppRegistry,
    Text,
    Dimensions,
    View,
    Button,
    Platform,
    TouchableHighlight,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons'

class Separator extends React.PureComponent {
    render() {
        return(
            <View style={{
                padding: 5,
                backgroundColor:'#3b5998',
                alignItems:'center',
                borderRadius: 4,
            }}><Text style={{color:'white'}}> Anunciantes </Text>
            </View>
        )
    }
}

class ContactsListCard extends React.PureComponent {

    render() {

        let {width, height} = Dimensions.get('window')

        return (
            <View>
                <View style={{
                    padding: 5,
                }}>
                    <TouchableHighlight
                        activeOpacity={0.70}
                        underlayColor={'white'}
                        onPress={this.props.onPress}
                        style={{
                            padding: 5,
                            flex: 1,
                            backgroundColor: 'white',
                            borderRadius: 4,
                            shadowColor: 'black',
                            shadowOffset: {width: 0, height: 1},
                            // shadowOpacity: 0.05,
                            // shadowRadius: 0.5,
                            // borderWidth: 1,
                            // borderColor: 'rgba(0, 0, 0, 0.06)'
                        }}>

                        <View style={{height: 40, flexDirection: 'row'}}>

                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                margin: 5,
                                marginRight: 10
                            }}>

                                <Image
                                    style={{width: 38, height: 38, borderRadius: 19}}
                                    source={require('../Images/tim.png')}/>

                            </View>

                            {/* <View style={{
                                backgroundColor:'#3b5998',
                                alignItems:'center',
                                justifyContent: 'center',
                                margin: 5,
                                }}><Text style={{color:'white'}}> Anunciantes </Text>
                            </View> */}


                            <View style={{
                                flex:1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                            }}>

                                <Text
                                    numberOfLines={1}
                                    style={{
                                        marginLeft: 0,
                                        fontSize: 14,
                                        color: '#ABABAB',
                                        fontWeight: !this.props.familyName ? 'bold' : null
                                    }}>
                                    { `${this.props.givenName} `}
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            marginLeft: 0,
                                            fontSize: 14,
                                            color: '#ABABAB',
                                            fontWeight: 'bold',
                                        }}>
                                        { `${this.props.familyName} `}
                                    </Text>
                                </Text>

                                {/* <Text numberOfLines={2} style={{
                                    fontSize: 16,
                                    fontWeight: 'bold',
                                    color:'#3b5998',
                                    top: 5
                                    }}>
                                    { this.props.telefone }
                                </Text> */}

                            </View>

                            {/* <Icon name="chevron-right" size={25} style={{top:6}} color={'#eda631'} /> */}
                            <Icon name="chevron-right" size={25} style={{top:6}} color={'#ccc'} />

                        </View>

                    </TouchableHighlight>
                </View>
                <View style={{backgroundColor:'#ccc', top:2, height:0.4, width: width}}></View>

            </View>
        )
    }
}


export default ContactsListCard;
