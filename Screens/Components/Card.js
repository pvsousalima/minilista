import React from 'react';
import {
    AppRegistry,
    Text,
    View,
    Button,
    Platform,
    TouchableHighlight,
    Image
} from 'react-native';

class Separator extends React.Component {
    render() {
        return(
            <View style={{
                padding: 5,
                backgroundColor:'#3b5998',
                alignItems:'center',
                borderRadius: 4
            }}><Text style={{color:'white'}}> Anunciantes {this.props.tipoAnunciante} </Text>
            </View>
        )
    }
}

class Card extends React.Component {

    renderSeparator(){
        if(this.props.startGroup){
            return(
                <Separator tipoAnunciante={this.props.tipoAnunciante}/>
            )
        }
    }

    selectAnuncianteColor(){
        if(this.props.tipoAnunciante === 'ouro') return '#eda631'
        if(this.props.tipoAnunciante === 'prata') return '#C0C0C0'
        if(this.props.tipoAnunciante === 'bronze') return '#cd7f32'
    }

    render() {
        return (
            <View style={{
                padding: 10,
                paddingBottom: 0,
            }}>
                <TouchableHighlight
                    activeOpacity={0.70}
                    onPress={this.props.onPress}
                    underlayColor={'white'}
                    style={{
                        padding: 10,
                        flex: 1,
                        backgroundColor: 'white',
                        borderRadius: 4,
                        shadowColor: 'black',
                        shadowOffset: {width: 0, height: 1},
                        shadowOpacity: 0.05,
                        shadowRadius: 0.5,
                        borderWidth: 1,
                        borderColor: 'rgba(0, 0, 0, 0.09)',
                    }}>
                    <View>
                        <View style={{height: 40, flexDirection: 'row'}}>

                            {/* <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                margin: 5,
                                }}>

                                <Image
                                style={{top:10, width: 58, height: 58, borderRadius: 29}}
                                source={{uri: this.props.image }}/>

                            </View> */}

                            <View style={{
                                flex:1,
                                height: 40,
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                                marginLeft: 2
                            }}>
                                <Text
                                    numberOfLines={1}
                                    style={{
                                        marginLeft: 0,
                                        marginRight: 5,
                                        fontSize: 14,
                                        backgroundColor: 'transparent',
                                        color: '#ABABAB',
                                        top:5
                                    }}>
                                    <Text style={{
                                        fontSize: 23,
                                        fontWeight: 'bold',
                                        color:'black'
                                        // color: '#27976D'
                                    }}>
                                        { this.props.nome }
                                    </Text>
                                </Text>
                                <Text numberOfLines={2} style={{
                                    fontSize: 22,
                                    fontWeight: 'bold',
                                    color:'#3b5998',
                                    top: 5
                                }}>
                                    { this.props.telefone }
                                </Text>
                            </View>


                            <View style={{
                                    height: 40,
                                    backgroundColor:'red'
                            }}>
                                {/* <Text numberOfLines={1} style={{
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                    color: this.selectAnuncianteColor(),
                                    top: 5
                                    }}>{this.props.tipoAnunciante? `${this.props.tipoAnunciante.toLowerCase()}` : null}
                                </Text> */}
                            </View>
                            
                        </View>


                        <View style={{marginBottom: 5}}>
                            <Text
                                numberOfLines={2}
                                style={{
                                    backgroundColor: 'transparent',
                                    padding: 5,
                                    paddingTop: 5,
                                    fontSize: 22,
                                    fontWeight: 'bold',
                                }}>{this.props.title}</Text>
                            <Text
                                numberOfLines={2}
                                style={{
                                    backgroundColor: 'transparent',
                                    padding: 5,
                                    paddingBottom: 4,
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                }}>{`${this.props.endereco}`}</Text>

                            <Text
                                numberOfLines={4}
                                style={{
                                    backgroundColor: 'transparent',
                                    padding: 5,
                                    paddingTop: 0,
                                    paddingBottom: 0,
                                    fontSize: 15,
                                    color: '#6C836E'
                                }}>{this.props.descricao}</Text>
                        </View>

                    </View>

                </TouchableHighlight>
            </View>
        )
    }
}


export default Card;
