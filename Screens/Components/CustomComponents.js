import React, {PureComponent} from 'react'

import {
    AsyncStorage,
    Dimensions,
    StatusBar,
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native'

import {
    SearchBar,
    List,
    ListItem,
    SideMenu
} from 'react-native-elements'

import {customToastAlert, cacheToAsyncStorage, readFromAsyncStorage} from '../../API'
import Icon from 'react-native-vector-icons/MaterialIcons'
import FBSDK from 'react-native-fbsdk'

const {
    LoginButton,
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager,
} = FBSDK

let {width, height} = Dimensions.get('window')

export class Login extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {},
            logged: false
        }
        this.user = this.state.user
    }

    componentWillMount(){
        readFromAsyncStorage('logged').then((logged) => {
            if (logged) {
                readFromAsyncStorage('profile').then((user) => {
                    this.setState({user: user, logged: logged})
                })
            }
        })
    }

    renderProfile(){
        if (this.state.logged) {
            return (
                <View style={{alignItems:'center',margin:10}}>
                    <Image
                        style={{width: 90, height: 90, borderRadius:45, shadowOpacity:5}}
                        source={{uri: this.state.user.picture}}
                    />
                    <Text style={{margin:10, color:'white', fontSize:22, 'fontWeight':'600', marginBottom:15}}>{`${this.state.user.name}`}</Text>
                </View>
            )
        }
    }

    facebookLogin(){
        // Start the graph request.
        AccessToken.getCurrentAccessToken().then((data) => {
            new GraphRequestManager().addRequest(
                new GraphRequest(
                    '/me',
                    {
                        accessToken: data.accessToken,
                        parameters: {
                            fields: {
                                string: 'email,name,first_name,middle_name,last_name'
                            }
                        }
                    },
                    (error: ?Object, result: ?Object) => {
                        if (error) {
                            customToastAlert('Erro', 'Erro ao carregar informacões: ' + error.toString())
                        } else {
                            let user = result
                            user.picture = "https://graph.facebook.com/"+user.id+"/picture?height=180"
                            cacheToAsyncStorage('profile', user).then(() => {
                                cacheToAsyncStorage('logged', true).then(() => {
                                    this.setState({user: user, logged: true})
                                })
                            })
                        }
                    },
                )
            ).start()
        })
    }

    render() {
        return (
            <View style={{justifyContent:'center', alignItems:'center'}}>

                {this.renderProfile()}

                <LoginButton
                    publishPermissions={["publish_actions"]}
                    onLoginFinished={
                        (error, result) => {
                            if (error) {
                                customToastAlert('Erro', 'Erro no login: ' + result.error)
                            } else if (result.isCancelled) {
                                customToastAlert('Erro', 'Login cancelado.')
                            } else {
                                this.facebookLogin()
                            }
                        }
                    }
                    onLogoutFinished={ async () => {
                        try {
                            await AsyncStorage.removeItem('profile');
                            cacheToAsyncStorage('logged', false).then(() => {
                                this.setState({logged: false})
                                customToastAlert('Logout', 'Você saiu do facebook.')
                            })
                        } catch (error) {
                            customToastAlert('Erro', 'remove item')
                        }
                    }}/>

                </View>
            )
        }
    }

    export const MyStatusBar = ({backgroundColor, ...props}) => (
        <StatusBar backgroundColor={backgroundColor} {...props} />
    )

    const fetchNewDataSourceFromSearch = (props, text) => {
        if(props.state){
            if(text.length > 0) {
                let newDataSource = []
                props.state.data.map((el) => {
                    if(text !== '' && el.nome.toLowerCase().includes(text.toLowerCase())){
                        newDataSource.push(el)
                    }
                })
                return newDataSource
            }
        }
    }

    export class CustomHeaderWithSearchBar extends React.Component{
        constructor() {
            super()
            this.state= {
                clearIcon: {color: 'transparent'}
            }
        }

        render() {
            return (

                <View style={{backgroundColor: '#3b5998', paddingTop:24, paddingBottom:10}}>

                    <View style={{ alignItems:'flex-start', 'flexDirection': 'row', top:2}}>

                        <TouchableOpacity style={{marginLeft:15, width:30, height:30, top:3}} onPress={() => {
                            this.props.toggleSideMenu()
                        }}><Icon name="menu" size={25} style={{justifyContent:'center'}} color={'white'}/>
                        </TouchableOpacity>

                        <SearchBar
                            lightTheme
                            containerStyle={{borderRadius: 5, height: 30, width:width-70, backgroundColor:'white', marginRight:10, marginLeft:15}}
                            inputStyle={{backgroundColor: 'white', height: 15}}
                            icon={{color: '#86939e', name: 'search', style:{top:7} }}
                            clearIcon={this.state.clearIcon}
                            placeholder='Pesquisar na minilista'
                            textInputRef='minilista'
                            onFocus={() => {
                                this.setState({clearIcon: {color: '#86939e', name: 'clear', style:{top:7}}})
                            }}
                            onBlur={()=> {
                                this.setState({clearIcon: {color: 'transparent', name: 'clear'}})
                            }}
                            onChangeText={(text) => {
                                if(this.props.state){
                                    if(text.length > 0){
                                        this.props.updateDataSource(fetchNewDataSourceFromSearch(this.props, text))
                                    } else {
                                        this.props.updateDataSource(this.props.state.data)
                                    }
                                }
                            }}
                        />

                    </View>
                </View>
        )
    }
}
