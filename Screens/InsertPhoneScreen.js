
import React from 'react';
import {
    Text,
    TextInput,
    View,
} from 'react-native';

import {NavigationActions} from 'react-navigation';
import {Button} from 'react-native-elements'
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {mtel, readFromAsyncStorage, cacheToAsyncStorage} from '../API'

const backAction = NavigationActions.back({
    key: null
})

class InsertPhoneScreen extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null,
    })

    constructor(){
        super()
        this.state = {
            text:''
        }
    }

    componentWillMount(){
        readFromAsyncStorage('telephone').then((telephone) => {
            telephone ? this.setState({text:telephone}) : null
        })
    }

    render() {
        return(
            <View style={{flex: 1, backgroundColor:'#3b5998', justifyContent:'center'}}>
                <View style={{flex:1, margin:10, marginTop:37}}>
                    <View style={{justifyContent:'center', alignItems:'center', margin: 10}}>
                        <Text style={{ color:'white', fontWeight:'600'}}>
                            Insira seu número de telefone e DDD!
                        </Text>
                    </View>
                    <TextInput
                        autoFocus={true}
                        keyboardType={'numeric'}
                        placeholder="digite o seu telefone"
                        style={{textAlign: 'center', textAlignVertical: 'center',fontWeight: 'bold',height: 60, margin:10, color:'white', fontSize:28, padding: 4}}
                        onChangeText={(text) => this.setState({text:(mtel(text))})}
                        value={this.state.text}/>
                </View>
                { this.state.text ?
                    <Button
                        textStyle={{color: '#3b5885'}}
                        buttonStyle={{backgroundColor:'white', margin:30, borderRadius:4}}
                        icon={{color: '#3b5885', name: 'phone', type: 'MaterialIcons'}}
                        title='Confirmar telefone'
                        onPress={() => {
                            cacheToAsyncStorage('ddd', this.state.text.substring(1,3)).then(() => {
                                cacheToAsyncStorage('telephone', this.state.text).then(() => {
                                    if(this.props.navigation.state.params.refreshSideMenu){
                                        this.props.navigation.state.params.refreshSideMenu()
                                        this.props.navigation.dispatch(backAction)
                                    }
                                })
                            })
                        }}
                        /> : null
                    }
                    <KeyboardSpacer/>
                </View>
            )
        }
    }


    export default InsertPhoneScreen;
