import React from 'react';
import {
    Animated,
    AppRegistry,
    Dimensions,
    Text,
    View,
    Button,
    Image,
    RefreshControl,
    ScrollView,
    AsyncStorage,
    StatusBar,
    ListView,
    TouchableOpacity,
    TextInput
} from 'react-native';

import {StackNavigator} from 'react-navigation';
import {List, ListItem, SearchBar, Divider} from 'react-native-elements'
import {Card} from './Components'
import faker from 'faker'
import Icon from 'react-native-vector-icons/MaterialIcons'
import * as Animatable from 'react-native-animatable';

const {width, height} = Dimensions.get('window')

class EditableWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderAction() {
        if (this.state.editMode) {
            return <TextInput {...this.props}
                underlineColorAndroid = {'transparent'}
                style={{flex: 1, height: 40, fontSize: 16, paddingLeft: 10, color:'#000000'}}
                placeHolder={this.props.actionTitle}/>
        }
    }

    render() {
        return (
            <TouchableOpacity
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <Text style={{color: 'white', fontSize:16}}>{this.props.actionTitle}</Text>
                {this.renderAction()}
            </TouchableOpacity>
            )
    }
}

export class Separator extends React.Component {
    render() {
        return (
            <View style={{
                width: this.props.width,
                backgroundColor: 'rgba(0, 0, 0, 0.05)',
                height: 1,
                alignSelf: 'flex-end'
            }}/>
        )
    }
}

class Group extends React.Component {
    render() {
        return (
            <View
                style={{
                    backgroundColor: 'rgba(500, 500, 500, 0.06)',
                    marginTop: 5,
                    marginBottom: 5,
                }}
                {...this.props}
            >
                { this.props.children }
            </View>
        )
    }
}

class ProfileScreen extends React.Component {

    static navigationOptions = {
        headerStyle: {
            backgroundColor: '#3b5998',
        },
        headerTintColor:'white'
    }

    constructor(props){
        super(props)
        this.state = {}
    }


    render() {

        const {navigate} = this.props.navigation
        this.props = this.props.navigation.state.params

        return(
            <View key={'main'} style={{flex:1, height: height - 64, width: width, left: 0, bottom: 0, backgroundColor: '#F7F7F7'}}>

                <View style={{backgroundColor:'#3b5885', flex: 1}}>

                    <View style={{flex:1}}>
                        <ScrollView>
                            <Animatable.View animation="fadeIn" duration={2000} style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 5,
                                paddingTop: 20,
                                padding: 10,
                                flexDirection:'row',
                                backgroundColor:'#263f6a'
                            }}>
                                <Image
                                    style={{width: 60, height: 60, borderRadius: 30, margin: 5}}
                                    source={{uri: this.props.image}}/>

                                <Text numberOfLines={2} style={{
                                    marginLeft: 10,
                                    fontWeight: '300',
                                    fontSize: 26,
                                    color: 'white'
                                }}> {this.props.nome}  </Text>

                            </Animatable.View>

                            <Group>
                                <Separator width={width - 50}/>
                                <EditableWidget
                                    editable={false}
                                    actionTitle={'Telefone'}
                                    value={this.props.telefone}/>
                                <Separator width={width - 50}/>
                                <EditableWidget
                                    editable={false}
                                    onChangeText={(lastName) => this.setState({lastName})}
                                    actionTitle={'Endereço'}
                                    value={this.props.endereco}/>
                                <Separator width={width - 50}/>
                                <EditableWidget
                                    editable={false}
                                    onChangeText={(city) => this.setState({city})}
                                    actionTitle={'Website'}
                                    value={this.props.website}/>
                            </Group>
                        </ScrollView>

                        <Animatable.View  animation="fadeIn" duration={1000} style={{backgroundColor:'white', alignItems:'center'}}>
                            <Animatable.Text >
                                <Text numberOfLines={2} style={{
                                    justifyContent:'center',
                                    fontWeight: '300',
                                    fontSize: 14,
                                    color: '#3b5885',
                                }}> Outros procuraram por {this.props.nome}  </Text>
                            </Animatable.Text>
                        </Animatable.View>

                    </View>


                </View>
            </View>
        )
    }
}


export default ProfileScreen;
