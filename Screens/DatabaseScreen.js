
import React from 'react';
import {
    AppRegistry,
    Dimensions,
    // Button,
    Text,
    View,
    RefreshControl,
    ActivityIndicator,
    AsyncStorage,
    ScrollView,
    Platform,
    ListView
} from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';

import {Card} from './Components';
import faker from 'faker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {CheckBox} from 'react-native-elements'
import { List, Button, ListItem } from 'react-native-elements'

const list = [
    {
        name: 'Rio Grande do Sul',
        subtitle: 'Cacota - RS'
    },
    {
        name: 'Minas Gerais',
        subtitle: 'Champinha - MG'
    }
]

const backAction = NavigationActions.back({
  key: null
})

class DatabaseScreen extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null,
    })

    constructor(){
        super()
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            checked:false,
            dataSource: this.ds.cloneWithRows(list)
        }
    }

    render() {
        return(
            <View style={{flex: 1, backgroundColor:'#3b5998', justifyContent:'center'}}>

                <View style={{flex:1, margin:10, marginTop:37}}>

                    <View style={{justifyContent:'center', alignItems:'center', margin: 10}}>

                        <Text style={{ color:'white', fontWeight:'600'}}>
                            Selecione a sua base telefônica favorita!
                        </Text>

                    </View>
                    {true ?
                        // {this.state.isLoading  === false ?
                        (<ListView
                            decelerationRate = 'normal'
                            enableEmptySections
                            showsVerticalScrollIndicator={false}
                            dataSource={this.state.dataSource}
                            renderRow={(data) =>

                                <ListItem
                                    roundAvatar
                                    // key={sectionID}
                                    title={data.name}
                                    subtitle={data.subtitle}
                                    containerStyle={{backgroundColor:'white'}}
                                    onPress={() => {
                                        // this.props.navigation.state.refreshSideMenu()
                                        this.props.navigation.dispatch(backAction)

                                    }}
                                />
                            }
                         />
                        ) : (<View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator/><Text >carregando</Text>
                        </View>)
                    }
                </View>

                {/* <View style={{flex:1, margin:15}}>
                    <List containerStyle={{marginBottom: 20}}>
                    {
                    list.map((l, i) => (
                    <ListItem
                    roundAvatar
                    key={i}
                    title={l.name}
                    subtitle={l.subtitle}
                    />
                    ))
                    }
                    </List>
                </View> */}


                {/* <View style={{marginBottom:10}}>
                    <Button
                    iconRight
                    // icon={{name: 'cached'}}
                    backgroundColor={'red'}
                    buttonStyle={{bottom:0}}
                    title='Avançar'
                    />
                </View> */}
            </View>
        )
    }
}


export default DatabaseScreen;
