import React from 'react';
import {
    AppRegistry,
    Text,
    View,
    Button,
    Platform,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';

import { StackNavigator, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Communications from 'react-native-communications';
import faker from 'faker';
import {readFromAsyncStorage, cacheToAsyncStorage, mergeWithAsyncStorage, mtel} from '../API'

const MyStatusBar = ({backgroundColor, ...props}) => (
    <StatusBar backgroundColor={backgroundColor} {...props} />
)

class DialerScreen extends React.Component {

    static navigationOptions = {
        header: null,
        visible:false,
        titleStyle: {
            color:'white'
        },
        tabBarIcon: ({ tintColor }) => (
            <Icon name="dialpad" size={23} color={tintColor} />
        ),
    }

    constructor(){
        super()
        this.state = {
            numberToCall:''
        }
    }

    pushToList(list, element){
        list.push(element)
        return list
    }

    createNewList(element){
        let newList = []
        newList.push(element)
        return newList
    }


    render() {
        // The screen's current route is passed in to `props.navigation.state`:
        const { params } = this.props.navigation.state;
        return (
            <View style={{flex:1, backgroundColor:'white'}}>

                <MyStatusBar backgroundColor="black" barStyle="light-content" />

                <View>

                    <View style={{ alignItems:'center', justifyContent:'center', flexDirection:'row'}}>

                        <View style={{top:40, flex:1, flexDirection:'row', padding: 10 }}>
                            <Icon style={{margin: 8}} color={'#6296f9'} name='add' size={25} />
                            <View style={{flex: 1}}>
                                <Text style={{fontSize:28, fontWeight:'300'}} textAlign={'center'} numberOfLines={1} ellipsizeMode={'head'}> {mtel(this.state.numberToCall)} </Text>
                            </View>
                            <TouchableOpacity style={{margin:8, height:25, width:25}}
                                onPress={() => {
                                    this.setState({numberToCall: this.state.numberToCall.slice(0, -1)})
                                }}>
                                <Icon color={'#6296f9'} name='backspace' size={25} />
                            </TouchableOpacity>
                        </View>

                    </View>


                    <View style={{top:60, flexDirection:'row', height:60, alignItems:'center', justifyContent:'center'}}>
                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'1'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/one.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'2'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/two.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'3'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/three.png')}/>
                        </TouchableOpacity>
                    </View>

                    <View style={{height:80, flexDirection:'row', top:70, alignItems:'center', justifyContent:'center'}}>
                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'4'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/four.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'5'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/five.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'6'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/six.png')}/>
                        </TouchableOpacity>
                    </View>

                    <View style={{height:80, flexDirection:'row', top:70, alignItems:'center', justifyContent:'center'}}>
                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'7'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/seven.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'8'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/eight.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'9'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/nine.png')}/>
                        </TouchableOpacity>
                    </View>

                    <View style={{height:80, flexDirection:'row', top:70, alignItems:'center', justifyContent:'center'}}>
                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'*'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/asterisk.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'0'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/zero.png')}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                this.setState({numberToCall:this.state.numberToCall+'#'})
                            }}>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/cerc.png')}/>
                        </TouchableOpacity>
                    </View>

                    <View style={{height:70, flexDirection:'row', marginTop: 80, alignItems:'center', justifyContent:'center'}}>
                        <TouchableOpacity style={{margin: 10, width: 70, height: 70}}
                            onPress={() => {
                                readFromAsyncStorage('historyList').then((list) => {
                                    if(list && list instanceof Array && list.length > 0){
                                        list.push({numberCalled: this.state.numberToCall, date: new Date()})
                                        cacheToAsyncStorage('historyList', list).then((saved) => {
                                            Communications.phonecall(this.state.numberToCall, false)
                                        })
                                    } else {
                                        cacheToAsyncStorage('historyList', this.createNewList({numberCalled: this.state.numberToCall, date: new Date()})).then((saved) => {
                                            Communications.phonecall(this.state.numberToCall, false)
                                        })
                                    }

                                })

                            }
                            }>
                            <Image
                                style={{width: 70, height: 70, borderRadius: 20}}
                                source={require('./Images/dialer/call.png')}/>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        )
    }
}

export default DialerScreen
