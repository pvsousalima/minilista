import React from 'react'
import {
    Alert,
    Animated,
    AppRegistry,
    Dimensions,
    Text,
    View,
    Button,
    RefreshControl,
    AsyncStorage,
    Platform,
    ScrollView,
    InteractionManager,
    TouchableOpacity,
    ListView
} from 'react-native';

import {
    SearchBar,
    List,
    ListItem,
    SideMenu
} from 'react-native-elements'

import {StackNavigator, NavigationActions} from 'react-navigation';
import {
    Card, MyStatusBar, Login, CustomHeaderWithSearchBar, Profile
} from './Components'

import faker from 'faker'
import Icon from 'react-native-vector-icons/MaterialIcons'

import {mtel, readFromAsyncStorage, cacheToAsyncStorage, customToastAlert} from '../API'

class HomeScreen extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        title: 'Minilista',
        header: (state) => {
            return  <CustomHeaderWithSearchBar {...navigation.state.params} />
        },
        tabBarIcon: ({ tintColor }) => (
            <Icon name="list" size={30} color={tintColor} />
        )
    })

    updateDataSource(newDataSource){
        this.setState({dataSource: this.state.dataSource.cloneWithRows(newDataSource)})
    }

    fillData(data){
        for(let i=0; i<10; i++) {
            data.push({
                nome: faker.name.findName(),
                telefone: '(31) 99327-8689',
                tipoAnunciante: 'ouro',
                endereco: faker.address.streetAddress(),
                image: faker.image.avatar(),
                website: 'http://google.com'
            })
        }

        for(let i=0; i<10; i++) {
            data.push({
                nome: faker.name.findName(),
                telefone: '(31) 99327-8689',
                tipoAnunciante: 'prata',
                endereco: faker.address.streetAddress(),
                image: faker.image.avatar(),
                website: 'http://google.com'
            })
        }

        for(let i=0; i<10; i++) {
            data.push({
                nome: faker.name.findName(),
                telefone: '(31) 99327-8689',
                tipoAnunciante: 'bronze',
                endereco: faker.address.streetAddress(),
                image: faker.image.avatar(),
                website: 'http://google.com'
            })
        }

        for(let i=0; i<10; i++) {
            data.push({
                nome: faker.name.findName(),
                telefone: '(31) 99327-8689',
                endereco: faker.address.streetAddress(),
                image: faker.image.avatar(),
                website: 'http://google.com'
            })
        }

        // this.data.sort(function(a,b) {
        //  if (a.Value1 === b.Value1) {
        //      if (a.Value2 === b.Value2) {
        //          return (a.Value3 < b.Value3) ? -1 : (a.Value3 > b.Value3) ? 1 : 0;
        //      } else {
        //          return (a.Value2 < b.Value2) ? -1 : 1;
        //      }
        //  } else {
        //       return (a.Value1 < b.Value1) ? -1 : 1;
        //  }
        //     });
    }

    constructor(props){
        super(props)

        //prepare data
        this.data = []
        this.fillData(this.data)

        // Listview Data source
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        // state
        this.state = {
            dataSource: this.ds.cloneWithRows([]),
            data: [],
            isOpen: false,
            options: [
                {
                    name: 'DDD',
                },
                {
                    name: 'Telefone',
                },
                {
                    name: 'Operadora',
                },
                {
                    name: 'Base de dados',
                    subtitle: 'Belo Horizonte'
                }
            ]
        }

        // function binders
        this.updateDataSource = this.updateDataSource.bind(this)
        this.onSideMenuChange = this.onSideMenuChange.bind(this)
        this.toggleSideMenu = this.toggleSideMenu.bind(this)
        this.refreshSideMenu = this.refreshSideMenu.bind(this)
    }

    passParams(params){
        this.props.navigation.setParams(params)
    }

    componentWillMount(){
        AsyncStorage.setItem('minilista', JSON.stringify(this.data)).then((saved) => {
            AsyncStorage.getItem('minilista').then((lista) => {

                this.setState({
                    data: JSON.parse(lista),
                    dataSource: this.ds.cloneWithRows(JSON.parse(lista))
                })

                this.passParams({
                    state: this.state,
                    updateDataSource: this.updateDataSource,
                    toggleSideMenu: this.toggleSideMenu,
                    onSideMenuChange: this.onSideMenuChange
                })

            }).catch(err => {
                this.setState({dataSource: this.ds.cloneWithRows([])})
            })
        })
        this.refreshSideMenu()
    }


    componentDidMount(){
    }

    onSideMenuChange (isOpen: boolean) {
        this.setState({
            isOpen: isOpen
        })
    }

    toggleSideMenu () {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    refreshSideMenu(){
        readFromAsyncStorage('ddd').then((ddd) => {
            readFromAsyncStorage('operator').then((operator) => {
                readFromAsyncStorage('telephone').then((telephone) => {
                    this.setState({options: [
                        {
                            name: 'DDD',
                            subtitle: ddd,
                            navigate: 'InsertPhoneScreen'
                        },
                        {
                            name: 'Telefone',
                            subtitle: telephone ? `+55 ${telephone}` : null,
                            navigate: 'InsertPhoneScreen'
                        },
                        {
                            name: 'Operadora',
                            subtitle: operator,
                            navigate: 'OperatorScreen'
                        },
                        {
                            name: 'Base de dados',
                            subtitle: 'Belo Horizonte',
                            navigate: 'DatabaseScreen'
                        }
                    ]})
                })
            })
        })


    }

    render() {

        let {width, height} = Dimensions.get('window')

        const {navigate} = this.props.navigation

        return(
            <SideMenu
                isOpen={this.state.isOpen}
                onChange={this.onSideMenuChange.bind(this)}
                menu={(
                    <View style={{flex: 1, backgroundColor: '#3b5885', paddingTop: 15, shadowOpacity:1, flexDirection:'column'}}>

                        <ScrollView showsVerticalScrollIndicator={false}>
                            {/* <Profile/> */}
                            <Login/>

                            <View style={{flexDirection:'column', marginTop:30}}>
                                <Text numberOfLines={1} style={{marginLeft:10, color:'white', fontWeight:'500'}}>
                                    Minhas configurações
                                </Text>
                                <List>
                                    {
                                        this.state.options.map((l, i) => (
                                            <ListItem
                                                roundAvatar
                                                onPress={()=>{this.props.navigation.navigate(l.navigate, {refreshSideMenu: this.refreshSideMenu})}}
                                                key={i}
                                                title={l.name}
                                                subtitle={l.subtitle}
                                            />
                                        ))
                                    }
                                </List>
                            </View>
                        </ScrollView>
                    </View>
                )}>

                <View key={'main'} style={{flex:1, height: height - 64, width: width, left: 0, bottom: 0, backgroundColor: '#F7F7F7', shadowOpacity: 0.3}} >
                    <MyStatusBar backgroundColor="#5E8D48" barStyle="light-content" />
                    <ListView
                        decelerationRate = 'normal'
                        enableEmptySections
                        removeClippedSubviews={false}
                        showsVerticalScrollIndicator={true}
                        refreshControl={
                            <RefreshControl
                                refreshing={false}
                                onRefresh={() => {
                                    // atualiza os dados da minilista
                                    setTimeout(() => { customToastAlert('Atualização', 'Sua minilista foi atualizada! :)') }, 1000);
                                }}
                            />
                        }
                        dataSource={this.state.dataSource}
                        renderRow={(data) =>
                            <Card {...data} onPress={() => {
                                this.props.navigation.navigate('ProfileScreen', data)
                            }}/>
                        }
                    />
                    <View style={{height:5}}/>
                </View>
            </SideMenu>

        )
    }
}


export default HomeScreen;
