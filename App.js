
import React, {PureComponent} from 'react'
import {
    AppRegistry,
    Text,
    View,
    Button,
    Platform,
    Image,
    StatusBar,
    ScrollView
} from 'react-native'

import {StackNavigator, TabNavigator} from 'react-navigation'
import {
    HomeScreen,
    DialerScreen,
    AllContactsScreen,
    ProfileScreen,
    DatabaseScreen,
    InsertPhoneScreen,
    OperatorScreen,
    RecentCallsScreen
} from './Screens'

const MainScreenNavigator = TabNavigator({
    Home: {
        screen: HomeScreen
    },
    AllContactsScreen: {
        screen: AllContactsScreen
    },
    Recents: {
        screen: RecentCallsScreen
    },
    Discador: {
        screen: DialerScreen
    },
},
{
    initialRouteName: 'AllContactsScreen',
    tabBarOptions: {
        labelStyle: {
            fontSize: 12
        },
        pressOpacity:0,
        style: {
            backgroundColor: 'white'
        },
        activeTintColor: '#3b5998',
        inactiveTintColor: '#ccc',
        indicatorStyle: {
            backgroundColor:'#3b5998'
        },
        showIcon: true
    }
})

const MoboApp = StackNavigator({
    DatabaseScreen: { screen: DatabaseScreen },
    InsertPhoneScreen: { screen: InsertPhoneScreen },
    Home: { screen: MainScreenNavigator },
    ProfileScreen:{ screen: ProfileScreen },
    OperatorScreen:{ screen: OperatorScreen },
},
// the stack navigator props, transitions, etc
{
    initialRouteName: 'Home',
    headerMode: 'screen',
    cardStyle: {
        opacity: 1,
        shadowOpacity: 0.2,
    },
    header: (props) => {},
}
)

//Should be pure... setState on top-level component doesn't seem to work
class TableViewExample extends React.PureComponent {
    render(){

        // Fill the full native table cell height.
        var style = {flex:1}

        // All Item props get passed to this cell inside this.props.data. Use them to control the rendering, for example background color:
        if (this.props.data.backgroundColor !== undefined) {
            style.backgroundColor = this.props.data.backgroundColor
        }

        return (
            <View style={{padding: 5}}>
                <View style={{margin:6, justifyContent: 'flex-start', flexDirection:'row', alignItems: 'flex-start'}}>
                    <Text numberOfLines={1} style={{fontSize: 16, color: '#ABABAB', fontWeight: !this.props.data.familyName ? 'bold' : null}}>
                        {`${this.props.data.givenName} `}
                        <Text numberOfLines={1} style={{fontSize: 16,  color: '#ABABAB', fontWeight: 'bold'}}>
                            {`${this.props.data.familyName}`}
                        </Text>
                    </Text>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('TableViewExample', () => TableViewExample);
AppRegistry.registerComponent('NavigationTest', () => MoboApp )
console.ignoredYellowBox = ['Warning: BackAndroid']
